﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TASK2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            //modify stack visibility
            StackMain.IsVisible = false;
            StackResult.IsVisible = false;
        }

        public void Continue(object Sender, EventArgs e)
        {
            //modify stack visibility
            StackInfo.IsVisible = false;
            StackMain.IsVisible = true;
        }

        public void Display(object Sender, EventArgs e)
        {
            int guess;

            //defaults for invalid answer
            StackResult.IsVisible = true;
            LabelResult.Text = "Input not valid";
            LabelResult2.Text = "You did not guess a valid number";
            StackContainer.BackgroundColor = Color.Black;

            //checks the guess is a number
            if (int.TryParse(EntryMain.Text, out guess) && guess < 6)
            {
                //if the number is valid compare the numbers in the compare class
                Compare.CompareNumbers(guess);
                //display the information from the compare class
                StackContainer.BackgroundColor = Color.FromHex(Compare.GetStrings["Colour"]);
                LabelResult.Text = $"The Number Was: {Compare.GetNumbers["Number"]}";
                LabelResult2.Text = Compare.GetStrings["Text"];
                LabelLeft.Text = $"Total Guesses: {Compare.GetNumbers["Guesses"]}";
                LabelRight.Text = $"Correct Guesses: {Compare.GetNumbers["Correct"]}";
            }
        }
    }
}
