﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace TASK2
{
    class Compare
    {
        static Dictionary<string, int> Numbers = new Dictionary<string, int>();
        static Dictionary<string, string> Strings = new Dictionary<string, string>();

        static Compare()
        {
            //sets up the dictionary of numbers to pass to the main class
            Numbers.Add("Number", 0);
            Numbers.Add("Correct", 0);
            Numbers.Add("Guesses", 0);
            //sets up the dictionary of strings to pass to the main class
            Strings.Add("Colour", "");
            Strings.Add("Text", "");
        }
        public static void CompareNumbers(int guess)
        {
            //generates a random number
            Numbers["Number"] = new Random().Next(1, 6);

            //changes the information to the default 'wrong' answer values
            Strings["Colour"] = "#ff0000";
            Strings["Text"] = "You got it wrong!";

            //adds to the number of guesses
            Numbers["Guesses"]++;

            //if the guess is correct
            if (Numbers["Number"] == guess)
            {
                //set information to the 'right' answer values
                Strings["Colour"] = "#00ff00";
                Strings["Text"] = "You got it right!";
                Numbers["Correct"]++;
            }
        }

        public static Dictionary<string, int> GetNumbers
        {
            //return the generated number, total guesses and correct guesses
            get
            {
                return Numbers;
            }
        }

        public static Dictionary<string, string> GetStrings
        {
            //return the background colour and text
            get
            {
                return Strings;
            }
        }
    }
}