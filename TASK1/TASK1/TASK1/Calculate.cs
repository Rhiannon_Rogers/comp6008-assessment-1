﻿namespace TASK1
{
    class Calculate
    {
       
        public static int CalculateTotal(int number)
        {
            int total = 0;

            //checks each number from 0 to the number given
            for (int i = 0; i < number; i++)
            {
                //if the number is divisible by 3 or 5, add the number to the total
                if (i % 3 == 0 || i % 5 == 0)
                {
                    total += i;
                }
            }

            //return the total
            return total;
        }
    }
}