﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TASK1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            //edit stack visibility
            StackResult.IsVisible = false;
            StackMain.IsVisible = false;
        }

        public void Continue(object Sender, EventArgs e)
        {
            //edit stack visibility
            StackInfo.IsVisible = false;
            StackMain.IsVisible = true;
        }
        public void Display(object Sender, EventArgs e)
        {
            int number;

            StackResult.IsVisible = true;

            //set up defaults for invalid input
            LabelResult.Text = "Not A Valid Input";
            LabelResult2.Text = "Not a number, or the number is too large";

            //check to see if valid number
            if (int.TryParse(EntryMain.Text, out number))
            {
                //if number is valid, display the total from the calculator class
                LabelResult.Text = "The Total Is:";
                LabelResult2.Text = Calculate.CalculateTotal(number).ToString();
            }
        }
    }
}
